# MapboxDeckglVizDemo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.17.

Written for this [post](https://calebukle.com/blog/use-mapbox-deckgl-angular-together-with-realtime-data)

To get this project working you'll need to get your own [Mapbox access token](https://account.mapbox.com/auth/signup/)

add your token to the `environment.ts`


If you have any issues raise an issue :) 
